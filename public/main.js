// objects
var cards, homecon;
// switches
var show = 'main';
var btoggle = false;
var itoggle = false;
// handles
var ho, ht;
var diff_list = []; 
var ediff_list = []; 
var difflevel;
var hdiff;

class Cards {
  constructor() {
    // this.data = [] // {o,t,id,diff,index,seen}
    this.pos = 0;
    this.indexlist = new Set(['alle']);
    this.index = "alle";
    this.data = {alle: []};
    this.deleted = [];
    this.editnow = -1;
  }
  get all() {
    let res = [];
    Object.keys(this.data).forEach(x => res = res.concat(this.data[x]));
    return res
  }
  set all(data) {
    for(let i=0;i<data.length;i++) {
      data[i].id = Cards.makeid(data[i].o,data[i].t);
      this.push(data[i]);
    }
  }
  get length() {
    return this.all.length
  }
  get diff() {
    return this.filter(this.index)[this.pos].diff
  }
  set diff(idiff) {
    this.filter(this.index)[this.pos].diff = idiff;
  }
  get now() {
    return this.filter(this.index)[this.pos]
  }
  get o() {
    return this.now.o
  }
  get cleano() {
    return this.now.o.replace("<em>","").replace("</em>","")
  }
  get cleant() {
    return this.now.t.replace("<em>","").replace("</em>","")
  }
  get t() {
    return this.now.t
  }
  get id() {
    return this.now.id
  }
  filter(index) {
    if (index === "alle")
      return this.all
    else
      return this.data[index]
  }
  push(card) {
    card.seen = 0;
    card.diff = 1;
    card.id = card.id || Cards.makeid(card.o, card.t);
    if (!card.index)
      card.index = "alle";
    else
      this.indexlist.add(card.index.toLowerCase());
    try {
      this.data[card.index].push(card);
    } catch(e) {
      this.data[card.index] = [card];
    }
    return card
  }
  del(id) {
    let card;
    if (id)
      card = this.find(id);
    else
      card = this.now;
    let pop = this.data[card.index].splice(this.data[card.index].findIndex(x => x.id === card.id),1);
    this.deleted.push(pop);
  }
  delindex(index) {
    if (index === "alle") // always keep this one
      this.data[index] = [];
    else {
      this.deleted.push(...this.data[index]);
      delete(this.data[index]);
      this.indexlist.delete(index);
    }
  }
  reindex(oldi, newi) {
    this.data[newi] = this.data[oldi];
    this.delindex(oldi);
    this.indexlist.add(newi);
    this.filter(newi).forEach( card => card.index = newi );
  }
  find(id) {
    return this.all.find(x => x.id === id)
  }
  swap(id) {
    let card = this.find(id);
    [card.o, card.t] = [card.t, card.o];
  }
  swapindex(index) {
    this.filter(index).forEach(function (card) {
      [card.o, card.t] = [card.t, card.o];
    });
  }
  showme() {
    card = this.now;
    card.seen++;
    return card
  }
  next() {
    let data_array;
    data_array = this.filter(this.index);
    this.pos++;
    if (this.pos >= data_array.length) {
      this.pos = 0;
      if (this.index !== "alle") Cards.shuffle(this.data[this.index]);
    }
    try {
      data_array[this.pos].seen++;
    } catch(e) {
      if (this.length === 0) {
        return {o: "-", t: ""}
      }
    }
    return data_array[this.pos]
  }
  prev() {
    let data_array;
    data_array = this.filter(this.index);
    this.pos--;
    if (this.pos < 0) this.pos = data_array.length-1;
    return data_array[this.pos]
  }
  restart(index) {
    this.index = index;
    this.shuffle(index);
    this.pos = 0;
  }
  forEach(f, params) {
    this.all.forEach(function (item) {
      f(item, params);
    });
  }
  shuffle(index) { // obj func
    //Cards.shuffle(this.filter(index));
    this.sorted_shuffle(this.filter(index));
  }
  sorted_shuffle(card_arr) {
    // adding random 0.x to int value; same seen value, random order
    card_arr.sort((a,b) => (a.seen-b.seen + (b.diff-a.diff)*2 + Math.random()));
  }
  static shuffle(data) {
    for (let i = data.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
      [data[i], data[j]] = [data[j], data[i]];
    }
  }
  static makeid(o,t) {
    return o + "_" + t;
  }
}

class HomeCon {
  constructor(hostname) {
    this.hostname = hostname;
    this.needtosync = false;
    this.lasttry = performance.now();
    this.max_nocon = 5*1000; // 3*60*10000
  }
  loadall(cards, callback) {
    this.run("", "", function (text) {
      let data = JSON.parse(text);
      cards.all = data;
      callback();
    });
  }
  importme(json_link) {
    this.run("import",json_link, function (text) {
      let data = JSON.parse(text);
      for(let i=0;i<data.length;i++) {
        data[i].id = Cards.makeid(data[i].o,data[i].t);
        cards.push(data[i]);
      }
    });
  }
  run(action, params, callback) {
    if (this.needtosync) {
      console.log(" - check")
      if (performance.now() - this.lasttry > this.max_nocon) {
        console.log(" - call sync")
        this.lasttry = performance.now();
        this.send("sync", JSON.stringify(cards.all));
      }
    }
    else {
      this.send(action, params, callback);
    }
  }
  send(action, params, callback) {
    let xhr = new XMLHttpRequest();
    let api_name = action; // add || sync
    let that = this;
    xhr.open('POST', this.hostname + '/api/' + api_name, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          that.needtosync = false;
          if (typeof(callback) === "function")
            callback(this.responseText);
        }
        else {
          that.needtosync = true;
          console.log(" needtosync true")
        }
      }
    }
    xhr.timeout = 3000;
    xhr.ontimeout = function () {
      that.needtosync = true;
      console.log(" needtosync true")
    }
    xhr.send(params);
  }
}

function loadme() {
  document.domain = "og8.org"
  // connect to parent in iframe
  try {
    api = parent.qd_api;
  } catch(e) {
    console.log("turtles cant talk to parent");
  }
  // define handles
  ho = document.getElementById('box1');
  ht = document.getElementById('box2');
  diff_list.push(document.getElementById('leicht'));
  diff_list.push(document.getElementById('ok'));
  diff_list.push(document.getElementById('schwer'));
  ediff_list.push(document.getElementById('eleicht'));
  ediff_list.push(document.getElementById('eok'));
  ediff_list.push(document.getElementById('eschwer'));
  hdiff = document.getElementById("difficulty");
  hindex = document.getElementById('divindex');
  hilabel = document.getElementById("editindex");
  //
  if (hilabel) hilabel.value = "alle";
  difflevel = hdiff ? parseInt(hdiff.value) : 1;
  select(difflevel);
  // init cards obj
  cards = new Cards();
  homecon = new HomeCon("https://study.og8.org");
  if (cards.length === 0) {
    ho.innerHTML = "...";
    ht.classList.add('whitening');
    select(difflevel);
    homecon.loadall(cards, function () {
      cards.indexlist.forEach( index => cards.shuffle(index) );
      next(); // first run
    });
  }
}

function add(o,t) { // interface for xt
  card = cards.push({o: o, t: t});
  homecon.run("add", JSON.stringify(card));
}

function showme(card) {
  card = card || cards.showme();
  closeit();
  ho.innerHTML = card.o;
  select(card.diff);
}

function next() {
  card = cards.next();
  showme(card);
}

function prev() {
  closeit();
  card = cards.prev();
  ho.innerHTML = card.o;
  select(card.diff);
}

function select(idiff, editmode) {
  list = editmode ? ediff_list : diff_list;
  difflevel = idiff;
  for(let i=0;i<3;i++) {
    if (i == idiff) list[i].classList.add("selected");
    else list[i].classList.remove("selected");
  }
}

function clickdiff(idiff, editmode) {
  select(idiff, editmode);
  cards.diff = idiff;
}

function delme(id, row) {
  cards.del(id);
  if (row) { // list view
    row.parentElement.removeChild(row);
  }
  else {
    next();
  }
  homecon.run("del",JSON.stringify(id || cards.id));
}

function editme(id) {
  id = id || cards.id;
  card = cards.find(id);
  toggle_view("edit");
  document.getElementById("ebox1").value = card.o;
  document.getElementById("ebox2").value = card.t;
  select(card.diff);
  cards.editnow = id;
}

function delindex(index, row) {
  cards.delindex(index);
  build_list();
  homecon.run("delindex",JSON.stringify(index));
}

function editindex(cell) {
  let index = cell.innerHTML;
  cell.innerHTML = "<input id='tmp' type='text' value="+cell.innerHTML+" class='inbox'></input>";
  document.getElementById('tmp').select();
  let tempfunc = function () { // adding to two events
    let newname = document.getElementById('tmp').value.toLowerCase();
    cell.innerHTML = newname;
    if (index !== newname) {
      cards.reindex(index,newname);
      homecon.run("reindex", JSON.stringify({oldi: index, newi: newname}));
    }
    build_list();
  };
  cell.addEventListener("keyup", (event) => {
    if (event.key === "Enter") {
      tempfunc();
    }
  });
  document.getElementById('tmp').addEventListener('focusout', tempfunc);
}

function create() {
  cards.editnow = -1; // in case there was a cancelled edit action
  toggle_view("edit");
  document.getElementById("ebox1").value = "";
  document.getElementById("ebox2").value = "";
}

function saveme() {
  if (document.getElementById("ebox1").value === "") return
  if (cards.editnow !== -1) {
    o = document.getElementById("ebox1").value;
    t = document.getElementById("ebox2").value;
    add(o,t,difflevel);
    // delete old one
    delme(cards.editnow);
    cards.editnow = -1;
    // done edit, go back
    toggle_view("main");
  }
  else { // create new
    o = document.getElementById("ebox1").value;
    t = document.getElementById("ebox2").value;
    add(o,t,difflevel);
  }
  // clear
  document.getElementById("ebox1").value = "";
  document.getElementById("ebox2").value = "";
}

function build_list() {
  // create index list
  let htable = document.getElementById("listindex");
  let col = htable.getElementsByClassName("list-row");
  for (let i = col.length - 1; i >= 0; --i) col[i].remove();
  cards.indexlist.forEach(function (index) {
    let row, cell, name_cell;
    row = document.createElement("div");
    row.className += "list-row";
    name_cell = document.createElement("div");
    name_cell.className += "list-cell wide-cell";
    name_cell.innerHTML = index;
    row.appendChild(name_cell);
    cell = document.createElement("div");
    cell.className += "list-cell icon";
    cell.innerHTML = '<img src="img/edit.png" height="15px" title="Rename index">';
    cell.onclick = () => editindex(name_cell);
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell icon";
    cell.innerHTML = '<img src="img/swap.png" height="15px" title="Swap cards">';
    cell.onclick = function () { cards.swapindex(index); build_list(); };
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell icon";
    cell.innerHTML = '<img src="img/delete.png" height="15px" title="Delete cards">';
    cell.onclick = () => delindex(index, row);
    row.appendChild(cell);
    htable.appendChild(row);
  });
  // create card list
  htable = document.getElementById("listitems");
  col = htable.getElementsByClassName("list-row");
  for (let i = col.length - 1; i >= 0; --i) col[i].remove();
  cards.forEach(function (card) {
    let row, cell;
    row = document.createElement("div");
    row.className += "list-row";
    cell = document.createElement("div");
    cell.className += "list-cell";
    cell.innerHTML = card.o;
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell";
    cell.innerHTML = card.t;
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell";
    cell.innerHTML = card.index;
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell";
    if (card.diff === 2) cell.classList.add("red");
    if (card.diff === 0) cell.classList.add("green");
    cell.innerHTML = card.seen;
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell icon";
    cell.innerHTML = '<img src="img/edit.png" height="15px" title="Edit card">';
    cell.onclick = () => editme(card.id);
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell icon";
    cell.innerHTML = '<img src="img/swap.png" height="15px" title="Swap cards">';
    cell.onclick = function () { cards.swap(card.id); build_list(); };
    row.appendChild(cell);
    cell = document.createElement("div");
    cell.className += "list-cell icon";
    cell.innerHTML = '<img src="img/delete.png" height="15px" title="Delete cards">';
    cell.onclick = () => delme(card.id, row);
    row.appendChild(cell);
    htable.appendChild(row);
  });
}

function importme() {
  homecon.importme(JSON.stringify({link: document.getElementById("importbox").value, indexname: document.getElementById("importindex").value}));
  toggle_view("main");
}

function toggle_view(show) {
  // main, list, edit
  show = show;
  if (show === "main") {
    document.getElementById("inner").style.display = "block";
    document.getElementById("listview").style.display = "none";
    document.getElementById("editview").style.display = "none";
    document.getElementById("importview").style.display = "none";
    cards.editnow = -1;
  }
  else if (show === "list") {
    document.getElementById("inner").style.display = "none";
    document.getElementById("listview").style.display = "block";
    document.getElementById("editview").style.display = "none";
    document.getElementById("importview").style.display = "none";
    build_list();
    cards.editnow = -1;
  }
  else if (show === "edit") {
    document.getElementById("inner").style.display = "none";
    document.getElementById("listview").style.display = "none";
    document.getElementById("editview").style.display = "block";
    document.getElementById("importview").style.display = "none";
  }
  else if (show === "import") {
    document.getElementById("inner").style.display = "none";
    document.getElementById("listview").style.display = "none";
    document.getElementById("editview").style.display = "none";
    document.getElementById("importview").style.display = "block";
  }
}

function capitalize(s) {
  return s[0].toUpperCase() + s.slice(1)
}

function toggle() {
  (btoggle ? closeit : showit)();
}

function toggle_index() {
  if (itoggle) {
    hindex.style.display = "none";
  }
  else {
    hindex.style.display = "block";
    // build index list
    for (let i = hindex.children.length - 1; i > 0; --i) hindex.children[i].remove();
    cards.indexlist.forEach(function (index) {
      let div = document.createElement("div");
      div.className += " difficulty";
      if (index === cards.index) div.classList.add("selected");
      div.innerHTML = index;
      div.onclick = () => select_index(index);
      hindex.appendChild(div);
    });
  }
  itoggle = !itoggle;
}

function select_index(index) {
  for (let i = hindex.children.length - 1; i > 0; --i) {
    let child = hindex.children[i];
    if (child.innerHTML === index)
      child.classList.add("selected");
    else
      child.classList.remove("selected");
  };
  cards.restart(index);
  showme();
  toggle_index();
}

function quickdict() {
  let query = btoggle ? cards.cleant : cards.cleano;
  api("search", query);
}

function showit() {
  ht.classList.remove('whitening');
  ht.innerHTML = cards.t;
  btoggle = true;
}

function closeit() {
  ht.classList.add('whitening');
  ht.innerHTML = ""; 
  btoggle = false;
}

function addEvent(element, eventName, fn) {
    if (element.addEventListener)
        element.addEventListener(eventName, fn, false);
    else if (element.attachEvent)
        element.attachEvent('on' + eventName, fn);
}

addEvent(window, 'load', function(){ loadme(); }); 


import os
import shutil
import json
from werkzeug.utils import secure_filename
from flask import Flask, flash, request, redirect, url_for, render_template
from peewee import *
from playhouse.shortcuts import model_to_dict
from bs4 import BeautifulSoup
import requests

app = Flask(__name__)


db = SqliteDatabase('cards.db')

class Cards(Model):
    o = CharField()
    t = CharField()
    id = CharField(unique=True)
    diff = IntegerField()
    index = CharField()
    class Meta:
        database = db

if not Cards.table_exists():
    db.create_tables([Cards])

@app.route('/api/', methods=['GET','POST'])
def listall():
    " IN : -, OUT : list of all cards "
    r = Cards.select().dicts()
    cards = [x for x in r] # list(r) wont work
    return json.dumps(cards)

@app.route('/api/add', methods=['POST'])
def add():
    """ IN : objects to add in json, {o:, t:, id:, diff:, index: }
        If card exists already, update the existing one.  """
    try:
        data = json.loads(request.data)
    except:
        print(" EE Cannot load json.")
        return "EE"
    #print(" request.data : ", data)
    try:
        Cards.create(**data)
    except IntegrityError:
        return update(data)
    return "OK"

@app.route('/api/del', methods=['POST'])
def rm():
    " IN : id to remove in json "
    try:
        id = json.loads(request.data)
    except:
        print(" EE Cannot load json.")
        return "EE"
    try:
        Cards.delete_by_id(id)
    except:
        print(" EE Cannot delete id : " + id)
        return "EE"
    return "OK"

@app.route('/api/delindex', methods=['POST'])
def rmindex():
    " IN : index to remove in json "
    try:
        index = json.loads(request.data)
    except:
        print(" EE Cannot load json.")
        return "EE"
    try:
        Cards.delete().where(Cards.index==index).execute()
    except Exception as e:
        print(" EE Cannot delete index : " + index)
        print(e)
        return "EE"
    return "OK"

@app.route('/api/sync', methods=['POST'])
def sync():
    " IN : all cards in json as list "
    data = json.loads(request.data)
    Cards.drop_table()
    db.create_tables([Cards])
    try:
        for card in data:
            Cards.create(**card)
    except Exception as e:
        return "EE"
    return "OK"

@app.route('/api/import', methods=['POST'])
def importme():
    " IN : link in json "
    print(request.data)
    data = json.loads(request.data) # link
    link = data["link"]
    indexname = data.get("indexname")
    if len(link) > 50 or not link.startswith("https://my.dict.cc/print/"):
        return "EE"
    r = requests.get(link)
    if r.status_code != 200:
        return "EE"
    soup = BeautifulSoup(r.text, features="html.parser")
    tab = soup.find_all("table")[1]
    trll = tab.find_all("tr")
    title = indexname or trll[0].text
    for tr in trll[2:]:
        tdll = tr.find_all("td")
        o = tdll[0].text
        t = tdll[1].text
        id = makeid(o,t)
        try:
            Cards.create(o=o, t=t, id=id, diff=1, index=title.lower())
        except IntegrityError:
            continue # card exists, maybe different index
    return listall()

@app.route('/api/reindex', methods=['POST'])
def reindex():
    " IN : oldi and newi in json "
    try:
        data = json.loads(request.data)
        oldi = data['oldi']
        newi = data['newi']
    except:
        print(" EE Cannot load json.")
        return "EE"
    try:
        Cards.update(index=newi).where(Cards.index==oldi).execute()
    except:
        print(" EE Cannot reindex.")
        return "EE"
    return "OK"

def update(data): # method
    " IN : card in json "
    try:
        card = Cards.get(id=data['id'])
        for k,v in data.items(): setattr(card, k,v)
        card.save()
        return "OK"
    except Exception as e:
        return "EE"

def makeid(o,t):
    return o + "_" + t

